const fs = require('fs');
const path = require('path');
const async = require('async');
const sharp = require('sharp');
const jimp = require('jimp');
const Tesseract = require('tesseract.js');
const tesseract = Tesseract.create({
    workerPath: path.join(__dirname, 'tesseract/src/node/worker.js'),
    langPath: path.join(__dirname, 'tesseract/langs/'),
    corePath: path.join(__dirname, 'tesseract/src/index.js'),
    lang: 'eng'
});

/* ------------------------- */

const whitelists = {
    name: '0123456789 ABCDEFGHIJKLMNOPQRSTUVWXYZ+-',
    number: '0123456789',
    cc: '0123456789 C'
};
const sourceDir = './source-images';
const sourceImages = fs.readdirSync(sourceDir);
const outputFile = './output/output.txt';
const tempDir = './temp';
const imageSettings = [
    {
        name: 'system-name',
        extract: {
            left: 768,
            top: 240,
            width: 740,
            height: 23
        },
        ccCrop: false,
        whitelist: whitelists.name
    },
    {
        name: 'fortify-trigger',
        extract: {
            left: 875,
            top: 663,
            width: 100,
            height: 23
        },
        ccCrop: false,
        whitelist: whitelists.number
    },
    {
        name: 'fortify-total',
        extract: {
            left: 853,
            top: 640,
            width: 100,
            height: 23
        },
        ccCrop: false,
        whitelist: whitelists.number
    },
    {
        name: 'undermine-trigger',
        extract: {
            left: 1298,
            top: 663,
            width: 100,
            height: 23
        },
        ccCrop: false,
        whitelist: whitelists.number
    },
    {
        name: 'undermine-total',
        extract: {
            left: 1274,
            top: 640,
            width: 100,
            height: 23
        },
        ccCrop: false,
        whitelist: whitelists.number
    },

    /// CC

    {
        name: 'cc-last',
        extract: {
            left: 1095,
            top: 304,
            width: 400,
            height: 28
        },
        ccCrop: true,
        whitelist: whitelists.cc
    },
    {
        name: 'cc-default',
        extract: {
            left: 1095,
            top: 343,
            width: 400,
            height: 28
        },
        ccCrop: true,
        whitelist: whitelists.cc
    },
    {
        name: 'cc-fortify',
        extract: {
            left: 1095,
            top: 382,
            width: 400,
            height: 28
        },
        ccCrop: true,
        whitelist: whitelists.cc
    },
    {
        name: 'cc-undermined',
        extract: {
            left: 1095,
            top: 421,
            width: 400,
            height: 28
        },
        ccCrop: true,
        whitelist: whitelists.cc
    },
    {
        name: 'cc-base',
        extract: {
            left: 1095,
            top: 461,
            width: 400,
            height: 28
        },
        ccCrop: true,
        whitelist: whitelists.cc
    },
];


let tempFiles = fs.readdirSync(tempDir);
let currentSystem = '';
let systemsDone = 0;
let ocrConfidence = 0;

/* -------------------------- */

// cuts image parts to buffer and passes it to the tesseract
function recognizeImage(image, settings) {
    return new Promise((resolve) => {
        sharp(image)
            .extract(settings.extract)
            .toBuffer()
            .then((data) => {
                ocrImageFromBuffer(data, settings).then(() => {
                    resolve();
                })
            });
    });
}

// recognizes text in image data
function ocrImageFromBuffer(data, settings) {
    return new Promise(resolve => {
        tesseract.recognize(data, {tessedit_char_whitelist: settings.whitelist})
            .then((result) => {

                // get basic result without whitespace characters
                let text = result.text.trim();

                // crop CC ... from string
                if (settings.ccCrop === true) {
                    text = text.split(' ')[0];
                }

                // save system name for progress console
                if (settings.name === 'system-name') {
                    currentSystem = text;
                }

                // adds current ocr confidence to the system overall confidence
                ocrConfidence += result.confidence;

                processInput(text + "\t").then(() => {
                    resolve();
                });
            });
    });
}

// save text to output file
function processInput(text) {
    return new Promise(resolve => {
        fs.open(outputFile, 'a', 666, function (e, id) {
            fs.write(id, text, null, 'utf8', () => {
                fs.close(id, () => {
                    resolve();
                });
            });
        });
    });
}

function bmp2png(image) {

    return new Promise((resolve, reject) => {
        jimp.read(sourceDir + '/' + image, (err, jimpImage) => {
            if (err) throw err;

            let newName = image.split('.')[0] + '.png';

            jimpImage.write(tempDir + '/' + newName, (err) => {
                if (err) {
                    console.log(err);
                    reject();
                } else {
                    resolve();
                }
            });
        });
    })
}

function clearTempDir() {
    return new Promise((resolve, reject) => {

        async.eachSeries(tempFiles, (image, clearTempCallback) => {
            try {
                fs.unlinkSync(tempDir + '/' + image);
            } catch (err) {
            }

            clearTempCallback();
        }, (err) => {
            if (err) {
                console.log(err);
                reject();
            } else {
                resolve();
            }
        });
    });
}

function convertSorceImages() {
    return new Promise((resolve, reject) => {
        async.eachSeries(sourceImages, (image, bmp2pngCallback) => {
            bmp2png(image).then(() => {
                bmp2pngCallback()
            });

        }, (err) => {
            if (err) {
                console.log(err);
                reject();
            } else {
                console.log('convert done');
                tempFiles = fs.readdirSync(tempDir);
                resolve();
            }
        });
    });
}

/* -------------------------- */

// remove old output file
try {
    fs.unlinkSync(outputFile);
} catch (err) {

}

clearTempDir().then(() => {
    console.log('temp cleared');

    convertSorceImages().then(() => {
        console.log('images converted');

        // loop over source files
        async.eachSeries(tempFiles, (image, imageCallback) => {

            // loop over image parts
            async.eachSeries(imageSettings, (settings, settingsCallback) => {

                recognizeImage(tempDir + '/' + image, settings).then(() => {
                    settingsCallback()
                });

            }, (err) => {
                if(err) console.log(err);

                // increase iteration counter
                systemsDone++;

                // progress in console
                console.info((systemsDone < 10 ? ' ' : '') + systemsDone + '/' + sourceImages.length + ' | ' + (ocrConfidence / 10).toFixed(1) + '% | Systém "' + currentSystem + '" Done!');

                // resets ocr confidence for next system
                ocrConfidence = 0;

                // adds new line
                processInput('\n');

                imageCallback();
            });

        }, (err) => {
            if(err) console.log(err);

            process.exit();
        });

    });
});